﻿#include <iostream>

void PrintOddOrEvenNumbers(int Limit, bool IsOdd)
{
    for (int i = IsOdd; i <= Limit; i += 2)
    {
        std::cout << i << "\n";
    }
}

int main()
{
    int N = 0;
    std::cout << "Input N:";
    std::cin >> N;
    for (int i = 0; i <= N;  i += 2) 
    {
        std::cout << i << "\n";
    }

    std::cout << "All odd numbers \n";
    PrintOddOrEvenNumbers(N, true);
    std::cout << "All even numbers \n";
    PrintOddOrEvenNumbers(N, false);

}